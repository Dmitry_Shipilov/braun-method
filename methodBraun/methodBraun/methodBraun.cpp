#include "stdafx.h"
#include <iostream>
#include <time.h>
#include <fstream>

#define ITER 10000
using namespace std;

// Объявление переменных
//----------------------------------------
const int rows = 3;		 // количество строк в платежной матрице
const int columns = 3;   // количество столбцов в платежной матрице

int K_count = -1;		 // количество итераций
double delta = 0.01;	 // заданная погрешность
double delta_K[ITER] = { 0 };		 // полученная разность, сравниваемая с "delta"

int Matrix[rows][columns] =			 // платежная матрица
{
	{9,8,4},
	{1,2,6},
	{0,0,0}
};

double i1_count = 0;		// количество раз выбрана стратегия 1 для игрока A
double i2_count = 0;
double i3_count = 0;

int i_sequence[ITER];	// последовательность выбранных стратегий для А

int i_current = 0;		// текущая стратегия для игрока A

double j1_count = 0;		// количество раз выбрана стратегия 1 для игрока B
double j2_count = 0;
double j3_count = 0;

int j_sequence[ITER];	// последовательность выбранных стратегий для В

int j_current = 0;		// текущая стратегия для игрока B

int M[ITER][columns] = { 0 };	// накопленный игроком A выигрыш за K партий, при условии, что В выбирает стратегию Bi 
int V[ITER][rows] = { 0 };	// накопленный игроком B проигрыш за K партий, при условии, что А выбирает стратегию Aj

double G2[ITER] = { 0 };		//	min M in a row
double maxG2[ITER] = { 0 };		//

double G1[ITER] = { 0 };	//	max V in a clolumn
double	minG1[ITER] = { 0 };	//

void showMas(void);
double findMax3(double num1, double num2, double num3); // поиск наибольшего из 3
double findMin3(double num1, double num2, double num3);	// поиск наименьшего из 3
void solveGame(void);

void showFullTable(int rows, int columns);	//показать таблицу целиком

ofstream out("out.txt");

int main()
{
	//showMas();
	solveGame();
	showFullTable(rows, columns);
	cout << rand() % (2 - 0) << endl;

	out.close();
	system("pause");
    return 0;
}

void showMas(void)
{

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			cout << Matrix[i][j] << " " ;
		}
		cout << endl;
	}
}

double findMax3(double num1, double num2, double num3)
{
	double max = -100000;
	if (max < num1)
		max = num1;
	if (max < num2)
		max = num2;
	if (max < num3)
		max = num3;
	return max;
}

double findMin3(double num1, double num2, double num3)
{
	srand(time(0));
	if ((num1 == num2) && (num1 == num3) && (num2 == num3))
	{
		return -100;
	}

	double min = 100000;
	if (min > num1)
		min = num1;
	if (min > num2)
		min = num2;
	if (min > num3)
		min = num3;
	return min;
}

void showFullTable(int rows, int columns)
{
	cout << "K " << "I_k  " << "    M   " << "G2 " << "J_k " << "  V  " << "   G1 " << "minG1 " << "maxG2 " << "deltaK " << endl;
	out << "K, I_k, ,M , ,  G2, J_k, ,V , ,  G1, minG1, maxG2, deltaK," << endl;

	for (int i = 0; i <= K_count; i++)
	{
		cout << i + 1 << "  " << i_sequence[i] + 1 << "   ";
		out << i + 1 << "," << i_sequence[i] + 1;

		cout << "|";
		out <<"," ;
		for (int j = 0; j < columns; j++)
		{
			cout << M[i][j] << " ";
			out << M[i][j] << ",";
		}
		cout << "|";
		cout << " " << round(G2[i]*100)/100 << "  " << j_sequence[i] + 1 << "  ";
		out << round(G2[i] * 100) / 100 << "," << j_sequence[i] + 1 << ",";
		cout << "|";
		for (int j = 0; j < rows; j++)
		{
			cout << V[i][j] << " ";
			out << V[i][j] << ",";
		}
		cout << "|";
		cout << round(G1[i]*100)/100 << "    " << minG1[i] << "    " << maxG2[i] << "      " << delta_K[i] << endl;
		out << round(G1[i] * 100) / 100 << "," << minG1[i] << "," << maxG2[i] << "," << delta_K[i] << "," << endl ;
		cout << "_____________________________________________" << endl;
	}

	cout << " p1* = " << i1_count << " / " << K_count + 1 << " = " <<  (i1_count / (K_count + 1) )<< endl;
	out << endl << "p1* = " << (i1_count / (K_count + 1)) << ",";

	cout << " p2* = " << i2_count << " / " << K_count + 1 << " = " <<  (i2_count / (K_count + 1)) << endl;
	out << endl << "p2* = " << (i2_count / (K_count + 1)) << ",";


	cout << " p3* = " << i3_count << " / " << K_count + 1 << " = " <<  (i3_count / (K_count + 1)) << endl;
	out << endl << "p3* = " << (i3_count / (K_count + 1)) << ",";


	cout << " q1* = " << j1_count << " / " << K_count + 1 << " = " << j1_count / (K_count + 1) << endl;
	out <<  endl << "q1 = " << (j1_count / (K_count + 1)) << ",";


	cout << " q2* = " << j2_count << " / " << K_count + 1 << " = " << j2_count / (K_count + 1) << endl;
	out << endl << "q2 = " << (j2_count / (K_count + 1)) << ",";


	cout << " q3* = " << j3_count << " / " << K_count + 1 << " = " << j3_count / (K_count + 1) << endl;
	out << endl << "q3 = " << (j3_count / (K_count + 1)) << ",";


	cout << "v = " << (maxG2[K_count] + minG1[K_count]) / 2 << endl;
	out << endl << "v = " << (maxG2[K_count] + minG1[K_count]) / 2 << endl;
}

void solveGame(void)
{
	srand(time(0));

	int current_row[3] = { 0 };
	int current_column[3] = { 0 };

	double G2maximum = -100000;
	double G1minimum = 100000;

	do
	{
		if (K_count < ITER)
		{
			A:

			K_count++;

			for (int j = 0; j < 3; j++)
			{
				current_row[j] += Matrix[i_current][j];
			}

			switch (i_current)
			{
			case 0:
			{
				i1_count++;
				break;
			}
			case 1:
			{
				i2_count++;
				break;
			}

			case 2:
			{
				i3_count++;
				break;
			}
			default:
				break;
			}	  // подсчет стратегий для игрока А

			switch (j_current)
			{
			case 0:
			{
				j1_count++;
				break;
			}
			case 1:
			{
				j2_count++;
				break;
			}

			case 2:
			{
				j3_count++;
				break;
			}
			default:
				break;
			}	  // подсчет стратегий для игрока B

			i_sequence[K_count] = i_current;

			//запоминаем текущую строку и столбец
			for (int i = 0; i < columns; i++)
			{
				M[K_count][i] = current_row[i];
			}

				// выбираем и считаем гамма1 и гамма2
			for (int i = 0; i <= K_count; i++)
			{
				if (G2maximum <= G2[i])
					G2maximum = G2[i];
			}

			if (findMin3(M[K_count][0], M[K_count][1], M[K_count][2]) != -100)
			{
				G2[K_count] = findMin3(M[K_count][0], M[K_count][1], M[K_count][2]) / (K_count + 1);

				for (int i = 0; i < columns; i++)
				{
					if (findMin3(M[K_count][0], M[K_count][1], M[K_count][2]) - M[K_count][i] == 0)
						j_current = i;
				}
				j_sequence[K_count] = j_current;
			}
			else
			{
				G2[K_count] = (M[K_count][0]) / (K_count + 1);

				j_current = rand() % (2 - 0);
				j_sequence[K_count] = j_current;

			}

			for (int j = 0; j < rows; j++)
			{
				current_column[j] += Matrix[j][j_current];
				V[K_count][j] = current_column[j];


			}

			G1[K_count] = findMax3(V[K_count][0], V[K_count][1], V[K_count][2]) / (K_count + 1);
			for (int i = 0; i <= K_count; i++)
			{
				if (G1minimum >= G1[i])
					G1minimum = G1[i];
			}
			for (int j = 0; j < rows; j++)
			{
				if (findMax3(V[K_count][0], V[K_count][1], V[K_count][2]) - V[K_count][j] == 0)
					i_current = j;
			}
			//

			// выбираем наибольший и наименьший гамма
			maxG2[K_count] = G2maximum;
			minG1[K_count] = G1minimum;
			//
			delta_K[K_count] = minG1[K_count] - maxG2[K_count];	// подсчет дельты
			

		} 
		else
			break;
	}while ((delta_K[K_count] > delta));
	cout << delta_K[K_count] << endl;
	
	cout << "Success" << endl;
	cout << K_count << endl;
}